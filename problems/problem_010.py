# Complete the is_divisible_by_3 function to return the
# word "fizz" if the value in the number parameter is
# divisible by 3. Otherwise, just return the number.
#
# You can use the test number % 3 == 0 to test if a
# number is divisible by 3.
#
# Pseudocode is there to guide you.


from tkinter.filedialog import test


def is_divisible_by_3(number):
    # If number is divisible by 3
    test(number) % 3 == 0
    if number % 3 == 0:
        return "fizz"
    else:
        return number
    pass
