# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.
#
# There is pseudocode to guide you.


def is_palindrome(word):
    # Reverse the word into a list of letters
    # "hello" becomes ["o", "l", "l", "e", "h"]
    reversed_list_of_letters = reversed(word)

    # Join the letters together using the empty string
    # ["o", "l", "l", "e", "h"] becomes "olleh"
    reversed_word = "".join(reversed_list_of_letters)

    if reversed_word == word:
        return True
    else:
        return False
    pass


# (edited)
# New

# Lynda Wellhausen she/her/hers  4:41 PM
#     # Reverse the word into a list of letters

#     # "hello" becomes ["o", "l", "l", "e", "h"]

#     reversed_list_of_letters = reversed(word)


#     # Join the letters together using the empty string

#     # ["o", "l", "l", "e", "h"] becomes "olleh"

#     reversed_word = "".join(reversed_list_of_letters)


#     return "this is the result"


# temp_word = "hello"

# is_palindrome(temp_word)

# print(is_palindrome(temp_word))
