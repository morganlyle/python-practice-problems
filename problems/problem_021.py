# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"
#
# There is pseudocode to guide you.


def gear_for_day(is_workday, is_sunny):
    # Create an empty list that will hold the different gear
    # gear = new empty list
    gear = []
    # If it is a workday and it is not sunny
    if is_workday != is_sunny:
        gear.append("umbrella")
        # Add "umbrella" to gear
        # gear.append("umbrella")
    if is_workday:
        gear.append("laptop")
    # If it is a workday
    # Add "laptop" to gear
    # Otherwise
    else:
        gear.append("surfboard")
    # Add "surfboard" to gear
    return gear
