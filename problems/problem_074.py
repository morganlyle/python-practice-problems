# Write a class that meets these requirements.
#
# Name:       BankAccount
#
# Required state:
#    * opening balance, the amount of money in the bank account
#
# Behavior:
#    * get_balance()      # Returns how much is in the bank account
#    * deposit(amount)    # Adds money to the current balance
#    * withdraw(amount)   # Reduces the current balance by amount
#
# Example:
#    account = BankAccount(100)
#
#    print(account.get_balance())  # prints 100
#    account.withdraw(50)
#    print(account.get_balance())  # prints 50
#    account.deposit(120)
#    print(account.get_balance())  # prints 170
#
# There is pseudocode for you to guide you.

class BankAccount:
    # method initializer(self, balance)
    def __init__(self, balance):
        # self.balance = balance
        self.balance = balance
    # method get_balance(self)

    def get_balance(self):
        # returns the balance
        return self.balance
    # method withdraw(self, amount)

    def withdraw(self, amount):
        # reduces the balance by the amount

        test_balance = self.balance - amount
        if test_balance > 0:
            self.balance = test_balance
            print(self.balance)
        else:
            print("value error")

        self.balance = self.balance - amount
        return self.balance
    # method deposit(self, amount)

    def deposit(self, amount):
        # increases the balance by the amount
        self.balance = self.balance + amount
        return self.balance


# # # create an instance of the BankAccount class
# account_one = BankAccount("500")

# # #  call get balance on the instance
# balance = account_one.get_balance()
# print("balance: ", balance)

# # call the withdraw method on instance and save the result to the new_balance variable

# new_balance = account_one.withdraw(25)

# print("new balance: ", new_balance)
